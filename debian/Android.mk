# shrinkedAndroid
# ===============

include $(CLEAR_VARS)
LOCAL_IS_HOST_MODULE := true
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := shrinkedAndroid
LOCAL_BUILT_MODULE_STEM := shrinkedAndroid.jar
LOCAL_MODULE_SUFFIX := $(COMMON_JAVA_PACKAGE_SUFFIX)

include $(BUILD_SYSTEM)/base_rules.mk

$(LOCAL_BUILT_MODULE): PRIVATE_PROGUARD_FLAGS:= \
  -include $(addprefix $(LOCAL_PATH)/, shrinkedAndroid.proguard.flags)
$(LOCAL_BUILT_MODULE): $(call java-lib-files,android_stubs_current) \
                       $(addprefix $(LOCAL_PATH)/, shrinkedAndroid.proguard.flags)| $(PROGUARD)
	@echo Proguard: $@
	$(hide) $(PROGUARD) -injars "$<(**/*.class)" -outjars $@ $(PRIVATE_PROGUARD_FLAGS)

INTERNAL_DALVIK_MODULES += $(LOCAL_INSTALLED_MODULE)

installed_shrinkedAndroid := $(LOCAL_INSTALLED_MODULE)